function filterEyeColorAgeAndFavoriteFruit(arr) {
	// body...

	// Tempat penampungan
	const result = [];

	// Perulangan mencari data perIndex
	for (let i = 0; i < arr.length; i++){

		// Kondisi dimana warna matanya biru dan agenya diantara 35 - 40 tahun, serta buah 
		// favoritenya apel
		if (arr[i].eyeColor === "blue" && (arr[i].age >= 35 && arr[i].age <= 40) &&
			arr[i].favoriteFruit === "apple" ) {
			result.push(arr[i]);
		}
	}

	// Kondisi jika data tidak ditemukan
	if (!result.length) {
		
		// Import file errorHandler
		const error = require('./errorHandler.js');

		// Ngepush errornya
		result.push(error);
		return result;
	}

	// Banyak data
	let totalData = result.length;

	// Data dan total data
	let Data = {
		"Total Data " : totalData,
		"Data": result
	}
	return Data;
}

// Import data
const data = require('./datas.js');

// Eksport data
module.exports = filterEyeColorAgeAndFavoriteFruit(data);