const Express = require('express');
const app = new Express();

const http = require('http');
const { PORT = 8000 } = process.env; // Ambil port dari environment variable

const fs = require('fs');
const path = require('path');
const PUBLIC_DIRECTORY = path.join(__dirname, 'public'); 

function getHTML(htmlFileName) {
  const htmlFilePath = path.join(PUBLIC_DIRECTORY, htmlFileName);
  return fs.readFileSync(htmlFilePath, 'utf-8')
}

const datas = require('./datas.js')
const data1 = require('./data1.js')
const data2 = require('./data2.js')
const data3 = require('./data3.js')
const data4 = require('./data4.js')
const data5 = require('./data5.js')

function onRequest(req, res) {
  switch(req.url) {
    case "/":
      res.writeHead(200)
      res.end(getHTML("homepage.html"))
      return;
    case "/about":
      res.writeHead(200)
      res.end(getHTML("about.html"))
      return;
    case "/data":
      const responseJSON = toJSON(datas)
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(responseJSON);
      return;
    case "/data1":
      const responseJSON1 = toJSON(data1)
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(responseJSON1);
      return;
    case "/data2":
      const responseJSON2 = toJSON(data2)
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(responseJSON2);
      return;
    case "/data3":
      const responseJSON3 = toJSON(data3)
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(responseJSON3);
      return;
    case "/data4":
      const responseJSON4 = toJSON(data4)
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(responseJSON4);
      return;
    case "/data5":
      const responseJSON5 = toJSON(data5)
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(responseJSON5);
      return;
    case "/asset/images/robot.jpg":
      const img = path.join(PUBLIC_DIRECTORY, "robot.jpg")
      fs.readFile(img, function(err, data) {
        // body...
        res.writeHead(404,{'Content-Type' : 'image/jpeg'});
        res.end(data)
      })
      return;
    default:
      res.writeHead(404)
      res.end(getHTML("404.html"))
      return;
  }
}

function toJSON(value) {
  return JSON.stringify(value);
}

const server = http.createServer(onRequest);

// Jalankan server
server.listen(PORT, '0.0.0.0', () => {
  console.log("Server sudah berjalan, silahkan buka http://0.0.0.0:%d", PORT);
})