function filterGenderCompanyAndAge(arr) {
	// body...

	// Tempat penampungan
	const result = [];

	// Perulangan mencari data perIndex
	for (let i = 0; i < arr.length; i++){

		// Kondisi dimana gendernya female, atau companynya FSW4 dan agenya diatas 30 Tahun
		if ((arr[i].gender === "female" || arr[i].company === "FSW4") && arr[i].age > 30) {
			result.push(arr[i]);
		}
	}
	
	// Kondisi jika data tidak ditemukan
	if (!result.length) {
		
		// Import file errorHandler
		const error = require('./errorHandler.js');

		// Ngepush errornya
		result.push(error);
		return result;
	}
	
	// Banyak data
	let totalData = result.length;

	// Data dan total data
	let Data = {
		"Total Data " : totalData,
		"Data": result
	}
	return Data;
}

// Import data
const data = require('./datas.js');

// Eksport data
module.exports = filterGenderCompanyAndAge(data);